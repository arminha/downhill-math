// Persistence of Vision Ray Tracer Scene Description File
// File: icy-title.pov
// Vers: 3.7
// Desc: Title with ice and snow
// Date: 08. 12. 2013
// Auth: Armin Häberling

// -w1600 -h250 +a0.3 +UA
// -w480 -h70 +a0.3 +UA

#version 3.7;

#include "colors.inc"
#include "textures.inc"
#include "functions.inc"

#include "makesnow2.inc"

#declare Photons = on;
#declare TransparentBackground = on;

global_settings {
  #if (Photons)
  photons {
    count 10000
    autostop 0
    jitter .4
    max_trace_level 7
  }
  #end
  max_trace_level 15
  assumed_gamma 1.0
}

// ----------------------------------------

camera {
  location <0,0,-15>
  look_at  <0,0,0>
  right    1.4*x
  up       1.4*y*image_height/image_width
}

light_source {
  //<2,15,-4>
  //<4,30,-8>
  <-5,5,-8>
  color rgb 1
  //parallel
  //point_at <0, 0, 0>
  #if (Photons)
    photons {
      reflection on
      refraction on
    }
  #end
}

// ----------------------------------------

#declare Particles = 20000;//15000;
#declare Size = 0.08;
#declare Thickness = 1;
#declare MinHeight = 1.1;
#declare MaxHeight = 1.3;
#declare Direction = -y+0.1*x;

#declare titleText =
text {
  ttf "../../fonts/flubber.ttf" "Downhill Math" 0.3, 0
  translate x*-3.1
  scale <3,3,1>
  translate y*-1.3
}

union {
object {
  titleText

  material { M_Glass2 }
  normal { bumps 0.1 scale 0.1 }
  #if (Photons)
  photons {
    target
    refraction on
    reflection on
  }
  #end

}

text {
  ttf "../../fonts/flubber.ttf" "Downhill Math" 0.28, 0
  translate x*-3.1
  scale <3,3,1>
  translate y*-1.3

  material {
    texture {
      pigment {
        #if (TransparentBackground)
          color rgbt <1,1,1,0.7>
        #else
          color rgbt <1,1,1,0.9>
        #end
      }
      finish {
        phong_size 10
        ambient 0.3
        diffuse 0.4
        phong 0.3
      }
      normal { 
	    bumps 0.2 scale 0.1
        //turbulence 0.5
	    //crackle 5 scale 0.075
        //turbulence 0.3
        //translate <-1,0,5>
      }
    }
    interior { ior 1.4 }
  }
  #if (Photons)
  photons {
    target
    refraction on
    reflection on
  }
  #end
}
}

object {
  MakeSnow(titleText,Particles,Size,Thickness,MinHeight,MaxHeight,Direction)
}

#if (!TransparentBackground)
plane {
  <0,0,1>, 1
  pigment {
    image_map {
      png "../../highscores/src/main/webapp/images/background.png"
      map_type 0
      interpolate 2
    }
    scale 10
  }
}
#end

