/*
 * Copyright (C) 2013 - 2014  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.rest.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.junit.Ignore;
import org.junit.Test;

import ch.corten.aha.scores.rest.Score;

public class ScoreResourceFunctionalTest {

    private static final String API_URL = "http://localhost:8080/api";

    @Test
    @Ignore
    public void testList() throws Exception {
        WebTarget scoresTarget = getScoresTarget();
        postScore(scoresTarget, newScore("player", 11417, 2));
        List<Score> scores = scoresTarget.request(
                MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<Score>>() { });
        assertNotNull(scores);
        assertNotEquals(0, scores.size());
    }

    @Test
    public void testCreate() throws Exception {
        WebTarget scoresTarget = getScoresTarget();
        Score score = newScore("player", 11417, 2);
        Score scoreReturn = postScore(scoresTarget, score);
        assertNotNull(scoreReturn);
        assertEquals(score.getName(), scoreReturn.getName());
        assertEquals(score.getErrors(), scoreReturn.getErrors());
        assertEquals(score.getTime(), scoreReturn.getTime());
    }

    private WebTarget getScoresTarget() {
        Client c = ClientBuilder.newClient();
        WebTarget apiRoot = c.target(API_URL);
        WebTarget scoresTarget = apiRoot.path("scores");
        return scoresTarget;
    }

    private Score newScore(String name, int time, int errors) {
        Score score = new Score();
        score.setName(name);
        score.setTime(time);
        score.setErrors(errors);
        return score;
    }

    private Score postScore(WebTarget scoresTarget, Score score) {
        Invocation builder = scoresTarget.request(MediaType.APPLICATION_JSON_TYPE).
                buildPost(Entity.entity(score, MediaType.APPLICATION_JSON_TYPE));
        return builder.invoke(Score.class);
    }
}
