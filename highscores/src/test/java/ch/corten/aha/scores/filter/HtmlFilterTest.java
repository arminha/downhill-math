/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.filter;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class HtmlFilterTest {

    private String filterInput;
    private String expectedResult;

    public HtmlFilterTest(String filterInput, String expectedResult) {
        this.filterInput = filterInput;
        this.expectedResult = expectedResult;
    }

    @Parameters(name = "{index}: {0}")
    public static Collection<Object[]> generateData() {
        return Arrays.asList(new Object[][] {
                {"hello world", "hello world"},
                {"<script>dosomethingbad();</script>", "&lt;script&gt;dosomethingbad();&lt;&#x2F;script&gt;"},
                {"<a href=\"http://example.com\">Click me</a>",
                        "&lt;a href=&quot;http:&#x2F;&#x2F;example.com&quot;&gt;Click me&lt;&#x2F;a&gt;"},
                {"hello \"world\"", "hello &quot;world&quot;"},
                {"hello 'world'", "hello &#x27;world&#x27;"},
                {"hello/world", "hello&#x2F;world"},
                {"hello w&#111;rld", "hello w&amp;#111;rld"},
        });
    }

    @Test
    public void sanitize() {
        HtmlFilter filter = new HtmlFilter();
        String result = filter.sanitize(filterInput);
        assertThat(result, is(expectedResult));
    }
}
