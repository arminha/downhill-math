/*
 * Copyright (C) 2014  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FilterChainTest {

    private FilterChain chain;
    private FilterMock filter1;
    private FilterMock filter2;

    @Before
    public void setup() {
        filter1 = new FilterMock("filter1");
        filter2 = new FilterMock("filter2");
        chain = new FilterChain(filter1, filter2);
    }

    @Test
    public void chainFilters() throws Exception {
        String input = "input";
        String output = chain.sanitize(input);
        assertEquals(input, filter1.getCapturedInput());
        assertEquals(filter1.getSanatizedString(), filter2.getCapturedInput());
        assertEquals(filter2.getSanatizedString(), output);
    }

    private static class FilterMock implements Filter {

        private final String sanitizedString;
        private String capturedInput;

        public FilterMock(String sanitizedString) {
            this.sanitizedString = sanitizedString;
        }

        @Override
        public String sanitize(String s) {
            capturedInput = s;
            return sanitizedString;
        }

        public String getSanatizedString() {
            return sanitizedString;
        }

        public String getCapturedInput() {
            return capturedInput;
        }
    }

}
