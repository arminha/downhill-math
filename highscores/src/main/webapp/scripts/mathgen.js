"use strict";

var quiz_gen = function(type, max, rand_gen) {
	if (!rand_gen) {
		rand_gen = {
			next : function() {
				return Math.random();
			},
			nextInt : function(from, to) {
				return Math.floor((Math.random() * (to - from)) + from);
			}
		};
	}
	max = (max != null) ? max : 12;
	var answers = [];
	for ( var i = 1; i <= max; i++) {
		for ( var j = i; j <= max; j++) {
			if (answers.indexOf(i * j) == -1) {
				answers.push(i * j);
			}
		}
	}
	answers.sort(function(a, b) { return a - b; });

	/**
	 * Return a random number from a given probability distribution.
	 */
	var random_from_dist = function(dist) {
		var all_weights = dist.reduce(function(a, b) { return a + b[1]; }, 0);
		var number = rand_gen.nextInt(0, all_weights);
		var sum_weight = 0;
		for ( var i = 0; i < dist.length; i++) {
			if (number >= sum_weight && number < sum_weight + dist[i][1]) {
				return dist[i][0];
			}
			sum_weight += dist[i][1];
		}
		return dist.slice(-1)[0][0];
	};

	var next_multiplication = function() {
		var fst = rand_gen.nextInt(1, max + 1);
		var snd = rand_gen.nextInt(1, max + 1);

		var result = fst * snd;
		var ind = answers.indexOf(result);
		var max_ind = Math.min(ind + 4, answers.length - 1);
		var min_ind = Math.max(max_ind - 9, 0);
		max_ind = Math.min(min_ind + 9, answers.length - 1);
		var other_ind = rand_gen.nextInt(min_ind, max_ind);
		if (other_ind >= ind) {
			other_ind++;
		}
		var false_result = answers[other_ind];
		var right = rand_gen.nextInt(0, 2) == 1;
		return {
			fst : fst,
			snd : snd,
			operator : "x",
			result : result,
			false_result : false_result,
			right : right,
			toString : function() {
				return this.fst + " " + this.operator + " " + this.snd;
			},
			left_answer : function() {
				return (right) ? this.false_result : this.result;
			},
			right_answer : function() {
				return (right) ? this.result : this.false_result;
			}
		};
	};
	var next_division = function() {
		var result = rand_gen.nextInt(1, max + 1);
		var divisor = rand_gen.nextInt(1, max + 1);
		var dividend = divisor * result;

		// probability distribution for the wrong answer
		var dist = [];
		for ( var n = 1; n <= max; n++) {
			var difference = Math.abs(n - result);
			if (difference < 5 && difference > 0) {
				dist.push([n, 5 - difference]);
			}
		}
		var false_result = random_from_dist(dist);
		var right = rand_gen.nextInt(0, 2) == 1;
		return {
			fst : dividend,
			snd : divisor,
			operator : "/",
			result : result,
			false_result : false_result,
			right : right,
			toString : function() {
				return this.fst + " " + this.operator + " " + this.snd;
			},
			left_answer : function() {
				return (right) ? this.false_result : this.result;
			},
			right_answer : function() {
				return (right) ? this.result : this.false_result;
			}
		};
	};
	var next_mixed = function() {
		var selector = rand_gen.nextInt(0, 2);
		if (selector > 0) {
			return next_multiplication();
		} else {
			return next_division();
		}
	};

	var next_func = next_multiplication;
	switch (type) {
	case "mult":
		next_func = next_multiplication;
		break;
	case "div":
		next_func = next_division;
		break;
	case "mixed":
		next_func = next_mixed;
		break;
	}

	return {
		next : next_func
	};
};