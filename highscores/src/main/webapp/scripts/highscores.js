
"use strict";

var scores = new function() {
	var apiUrl = "/api/scores";

	var showTime = function(centiseconds) {
		var cent = Math.floor(centiseconds % 100).toString();
		if (cent.length == 1) {
			cent = "0" + cent;
		}
		var sec = Math.floor((centiseconds % 6000) / 100).toString();
		if (sec.length == 1) {
			sec = "0" + sec;
		}
		var min = Math.floor(centiseconds / 6000).toString();
		return min + ":" + sec + "." + cent;
	};

	this.showScores = function(onCloseCallback) {
		var scores = $('#scores > tbody');
		var addLine = function(element) {
			var name = element.name;
			if (name.length > 20) {
				name = name.substring(0, 18) + '..';
			}
			// sanitize
			name = name.replace('<', '&lt;');
			name = name.replace('>', '&gt;');
			scores.append('<tr><td>' + element.rank + '</td><td>' + name + '</td><td>' + showTime(parseInt(element.time))
					+ '</td><td>' + element.errors + '</td></tr>');
		};
		scores.empty();
		// load highscores
		$.ajax({
			url: apiUrl,
			success: function(data) {
				scores.empty();
				data.forEach(addLine);
				var scoreboard = $('#scoreboard');
				scoreboard.overlay({
					// some mask tweaks suitable for modal dialogs
					mask: {
						color: '#111',
						loadSpeed: 200,
						opacity: 0.9
					},
					closeOnClick: true,
					onClose : onCloseCallback
				});
				scoreboard.overlay().load();
			},
			dataType: "json",
			cache: false
		});
	};

	var postScore = function(name, time, errors, onSuccessCallback) {
		var score = {
			'name' : name,
			'time' : time,
			'errors' : errors,
		};
		$.ajax({
			type : "POST",
			url  : apiUrl,
			contentType : "application/json; charset=UTF-8",
			dataType : "json",
			data: JSON.stringify(score),
			success : function() {
				// close overlay
				onSuccessCallback();
			},
			error : function() {
				// TODO show error
				// enable form again
				$('#name-form :input').removeProp('disabled');
			}
		});
	};

	this.noop = function() {};

	this.showEnterForm = function(time, errors, onSuccessCallback) {
		var nameForm = $('#name-form');
		// enable form
		nameForm.off('submit');
		$('#name-form :input').removeProp('disabled');
		nameForm.on('submit', function() {
			// disable form
			$('#name-form :input').prop('disabled', 'disabled');
			var name = nameForm.find('input[name="name"]').val();
			postScore(name, time, errors, onSuccessCallback);
		});
	};

	// send a request to appengine as soon as page is loaded to start an instance
	$(document).ready(function() {
		$.get(apiUrl, function() {}, "json");
	});
	var setScoreTableHeight = function() {
		$('#scoreboard div').css('max-height', Math.floor(window.innerHeight * 0.7) + 'px');
	};
	$(document).ready(setScoreTableHeight);
	$(window).resize(setScoreTableHeight);
}();



