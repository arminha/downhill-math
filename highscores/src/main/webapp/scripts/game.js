
"use strict";

var util = function() {
	var renderToCanvas = function (width, height, renderFunction) {
	    var buffer = document.createElement('canvas');
	    buffer.width = width;
	    buffer.height = height;
	    renderFunction(buffer.getContext('2d'));
	    return buffer;
	};

	var page_coordinates = function(gs, coord) {
		var canvasOffset = gs.pageOffset;
		var x = coord[0] + canvasOffset.left;
		var y = coord[1] + canvasOffset.top;
		return [x, y];
	};

	var canvas_coordinates = function(gs, coord) {
		var canvasOffset = gs.pageOffset;
		var x = coord[0] - canvasOffset.left;
		var y = coord[1] - canvasOffset.top;
		return [x, y];
	};

	var get_closest_touch_in_range = function(touches, coord, radius) {
		var touch = null;
		var min_dist = radius * radius;

		for ( var i = 0; i < touches.length; i++) {
			var xdiff = touches[i].pageX - coord[0];
			var ydiff = touches[i].pageY - coord[1];
			var dist = (xdiff*xdiff) + (ydiff*ydiff);
			if (dist < min_dist) {
				min_dist = dist;
				touch = touches[i];
			}
		}
		return touch;
	};

	var get_touch_with_id = function(touches, id) {
		if (id != null) {
			for ( var i = 0; i < touches.length; i++) {
				if (id == touches[i].identifier) {
					return touches[i];
				}
			}
		}
		return null;
	};

	return {
		page_coordinates : page_coordinates,
		canvas_coordinates : canvas_coordinates,
		get_closest_touch_in_range : get_closest_touch_in_range,
		get_touch_with_id : get_touch_with_id,
	}
}();

var isXmas = function() {
	var date = new Date();
	var month = date.getMonth();
	var day = date.getDate();
	return (month == 11 && day >= 20) || (month == 0);
}();

var roundRect = function (c, x, y, w, h, r) {
	if (w < 2 * r) r = w / 2;
	if (h < 2 * r) r = h / 2;
	c.beginPath();
	c.moveTo(x+r, y);
	c.arcTo(x+w, y,   x+w, y+h, r);
	c.lineTo(x+w, y-r);
	c.arcTo(x+w, y+h, x,   y+h, r);
	c.lineTo(x+r, y+h);
	c.arcTo(x,   y+h, x,   y,   r);
	c.lineTo(x, y+r);
	c.arcTo(x,   y,   x+w, y,   r);
	c.closePath();
};

var randomElements = function (max, n) {
	var elements = [];
	for ( var i = 0; i < max; i++) {
		elements.push(i);
	}
	var results = [];
	for ( var i = 0; i < n; i++) {
		var ind = Math.floor((Math.random() * elements.length));
		results.push(elements[ind]);
		elements.splice(ind, 1);
	}
	return results;
};

function Skier(world) {
	this.priority = 3;
	statemachine(this);

	this.world = world;

	/**
	 * x-coordinate of the skier. Midpoint of the skis.
	 */
	this.x = undefined;
	/**
	 * y-coordinate of the skier. Midpoint of the skis.
	 */
	this.y = undefined;

	this.trail = [];
	/**
	 * Direction of the skier as an angle. 0° is southwards.  
	 */
	this.dir = undefined;
	/**
	 * Current speed of the skier.
	 */
	this.v = undefined;

	this.fastRun = false;
	this.brake = false;
	var starting = false;

	var s;
	if (isXmas) {
		s = new Sprite(["center", "center"],
				{normal : [["images/xmas_skier.png"]],
				 brake : [["images/xmas_skier_braking.png"]],
				 fast : [["images/xmas_skier_fast.png"]],
				 start: [["images/xmas_skier_start01.png", 5],
			             ["images/xmas_skier_start02.png", 2],
			             ["images/xmas_skier_start03.png", 1],
			             ["images/xmas_skier_start04.png", 2],
			             ["images/xmas_skier_start05.png", 2],
			             ["images/xmas_skier_start06.png", 2],
			             ["images/xmas_skier_start07.png", 2]]
				},
				this._set_action);
	} else {
		s = new Sprite(["center", "center"],
				{normal : [["images/skier.png"]],
				 brake : [["images/skier_braking.png"]],
				 fast : [["images/skier_fast.png"]],
				 start: [["images/skier_start01.png", 5],
			             ["images/skier_start02.png", 2],
			             ["images/skier_start03.png", 1],
			             ["images/skier_start04.png", 2],
			             ["images/skier_start05.png", 2],
			             ["images/skier_start06.png", 2],
			             ["images/skier_start07.png", 2]]
				},
				this._set_action);
	}

	this._set_action = function() {
		if (starting) {
			return;
		}
		if (this.brake) {
			s.action("brake");
		} else if (this.fastRun) {
			s.action("fast");
		} else {
			s.action("normal");
		}
	};

	this.init = function(gs) {
		this.reset(gs);
		this.set_state("stand");
	};

	this.start = function() {
		starting = true;
		var n = 0;
		var that = this;
		s.action('start', true, function() {
			n++;
			if (n >= 4) {
				starting = false;
				that._set_action();
			}
		});
		this.set_state("ski");
	};

	this.reset = function(gs) {
		this.x = gs.width / 2;
		this.y = 0;
		this.dir = 0;
		this.trail = new Array();
		this.v = 0;
		this.fastRun = false;
	};

	this.draw = function(c, gs) {
		c.save();
		this.world.cameraTransformation(c);

		// draw trail
		if (this.trail.length > 0) {
			c.strokeStyle = "rgb(0,120,255)";
			c.beginPath();
			c.moveTo(this.x, this.y);
			for ( var i = this.trail.length - 1; i >= 0; i--) {
				c.lineTo(this.trail[i][0], this.trail[i][1]);
			}
			c.stroke();
		}

		// draw skier
		s.angle(-this.dir);
		s.draw(c, [this.x, this.y]);

		c.restore();
	};

	this.ski_update = function(gs) {
		// save trail
		if (this.trail.length > gs.framerate) {
			this.trail.shift();
		}
		this.trail.push([this.x, this.y]);

		// update velocity
		var cw = (this.fastRun && !this.brake) ? this.world.cwFast : this.world.cw;
		this.v = this.v + this.world.deltaT *
		          (this.world.gravity * Math.cos(this.dir) - cw * this.v * this.v);
		if (this.brake) {
			this.v = Math.max(this.v - this.world.deltaT * 10 * Math.sqrt(this.v), 0);
		}

		// update location
		var multiplicator = 5;
		var deltaX = this.v * Math.sin(this.dir) * this.world.deltaT * multiplicator;
		var deltaY = this.v * Math.cos(this.dir) * this.world.deltaT * multiplicator;
		if (this.world.slowdown > 0) {
			deltaX = deltaX * 0.5;
			deltaY = deltaY * 0.5;
		}
		this.x = this.x + deltaX;
		this.y = this.y + deltaY;
		s.update();
	};

	this.stand_update = function(gs) {
	};

	this.keyHeld_37 = function() {
		if (this.state == "ski") {
			var mult = (this.fastRun) ? 0.8 : 1.5;
			this.dir = Math.max(this.dir - this.world.deltaT * mult, -Math.PI/2);
		}
	};

	this.keyHeld_39 = function() {
		if (this.state == "ski") {
			var mult = (this.fastRun) ? 0.8 : 1.5;
			this.dir = Math.min(this.dir + this.world.deltaT * mult, Math.PI/2);
		}
	};

	this.keyUp_40 = function() {
		this.fastRun = false;
		this._set_action();
	};

	this.keyDown_40 = function() {
		this.fastRun = true;
		this._set_action();
	};

	this.keyUp_38 = function() {
		this.brake = false;
		this._set_action();
	};

	this.keyDown_38 = function() {
		this.brake = true;
		this._set_action();
	};

	this.get_collision_aabb = function() {
		return [this.x - 5, this.y - 5, 10, 10];
	};

	this.get_collision_circle = function() {
		return [[this.x, this.y], 5];
	};
}

function StopWatch(world) {
	this.priority = 6;
	this.seconds = 0;
	this.stopped = true;
	this.world = world;

	this.init = function(gs) {
	};

	var slowdown = new Sprite(["right", "top"], {normal : [["images/slow.png", 50], ["images/empty.png", 10]]});

	this.draw = function(c, gs) {
		var cent = Math.round(this.seconds.mod(1) * 100).toString();
		if (cent.length == 1) {
			cent = "0" + cent;
		}
		var sec = Math.floor(this.seconds.mod(60)).toString();
		if (sec.length == 1) {
			sec = "0" + sec;
		}
		var min = Math.floor(this.seconds / 60).toString();
		if (min.length == 1) {
			min = "0" + min;
		}

		var width = 98;
		var height = 29;
		c.save();
		c.font = "14pt flubberregular";
		c.fillStyle = "black";
		roundRect(c, gs.width - width - 8.5, -8.5, width + 16, height + 16, 9);
		c.fill();
		c.fillStyle = "white";
		roundRect(c, gs.width - width, 0, width, height, 4);
		c.fill();

		var topMargin = 21;
		var offset = 11;
		var spacing = 30;
		c.textAlign = "center";
		c.fillStyle = "black";
		c.fillText(cent, gs.width - spacing + offset, topMargin);
		c.fillText(".", gs.width - spacing + offset - 15, topMargin);
		c.fillText(sec, gs.width - 2 * spacing + offset, topMargin);
		c.fillText(":", gs.width - 2 * spacing + offset - 15, topMargin);
		c.fillText(min, gs.width - 3 * spacing + offset, topMargin);
		c.restore();

		if (this.world.slowdown > 0) {
			slowdown.draw(c, [gs.width - 112, 2]);
		}
	};

	this.update = function(gs) {
		if (!this.stopped) {
			this.seconds += this.world.deltaT;
		}
		slowdown.update();
	};

	this.start = function() {
		this.stopped = false;
	};

	this.stop = function() {
		this.stopped = true;
	};

	this.reset = function() {
		this.stopped = true;
		this.seconds = 0;
	};
}

function Gate(world, x, y, width, number, isleft) {
	this.priority = 2;
	this.x = x;
	this.y = y;
	this.width = width;
	this.world = world;
	this.number = number;
	this.color = isleft ? 'rgb(255, 0, 0)' : 'rgb(0, 0, 255)';
	this.correct_answer = false;
	this.passed = false;
	this.answer = 0;

	var height = isXmas ? 45 : 20;

	this.init = function(gs) {
	};

	this.is_visible = function() {
		return this.world.is_visible([this.x - 5,
		                            this.y - height,
		                            this.x + this.width + 5,
		                            this.y + height]);
	};

	var flag_sprite;
	if (isleft) {
		if (isXmas) {
			flag_sprite = new Sprite(["center", "bottom"],
					{left : [["images/cane_red_l.png"]],
					 right : [["images/cane_red_r.png"]]
			});
		} else {
			flag_sprite = new Sprite(["center", "bottom"],
				{normal : [["images/flagr01.png", 5],
				           ["images/flagr02.png", 5],
				           ["images/flagr03.png", 5],
				           ["images/flagr04.png", 5],
				           ["images/flagr05.png", 5],
				           ["images/flagr06.png", 5],
				           ["images/flagr07.png", 5],
				           ["images/flagr08.png", 5],
				]});
		}
	} else {
		if (isXmas) {
			flag_sprite = new Sprite(["center", "bottom"],
					{left : [["images/cane_blue_l.png"]],
					 right : [["images/cane_blue_r.png"]]
			});
		} else {
			flag_sprite = new Sprite(["center", "bottom"],
				{normal : [["images/flagb01.png", 5],
				           ["images/flagb02.png", 5],
				           ["images/flagb03.png", 5],
				           ["images/flagb04.png", 5],
				           ["images/flagb05.png", 5],
				           ["images/flagb06.png", 5],
				           ["images/flagb07.png", 5],
				           ["images/flagb08.png", 5],
				]});
		}
	}
	flag_sprite.set_frame(Math.floor((Math.random() * flag_sprite.get_num_frames())));

	var flag = function(c, x, y) {
		flag_sprite.draw(c, [x, y]);
	};

	this.draw = function(c, gs) {
		if (!this.is_visible()) {
			return;
		}

		c.save();
		c.strokeStyle = this.color;
		c.fillStyle = this.color;
		this.world.cameraTransformation(c);
		if (isXmas) {
			flag_sprite.action("left");
		}
		flag(c, this.x, this.y);
		if (isXmas) {
			flag_sprite.action('right');
		}
		flag(c, this.x + this.width, this.y);
		c.textAlign = "center";
		c.font = "18pt Arial";
		c.fillText(this.answer.toString(), this.x + this.width / 2, this.y);
		c.restore();
	};

	this.update = function(gs) {
		flag_sprite.update();
	};

	this.get_collision_aabb = function() {
		return [this.x, this.y - 5, this.width, 1];
	};

	this.collide_aabb = function(who, result) {
		this.passed = true;
	};
}

function FinishLine(world, gs) {
	this.priority = 1;
	this.passed = false;
	this.world = world;
	this.y = 0;

	this.draw = function(c, gs) {
		if (!this.world.is_visible([0, this.y - 2, 0, this.y + 30])) {
			return;
		}

		c.save();
		c.strokeStyle = 'rgb(255, 0, 0)';
		c.lineWidth = 2;
		this.world.cameraTransformation(c);
		c.beginPath();
		c.moveTo(this.world.border, this.y);
		c.lineTo(gs.width - this.world.border, this.y);
		c.stroke();

		c.font = "24pt Arial";
		c.fillStyle = 'rgb(255, 0, 0)';
		c.textAlign = "center";
		c.textBaseline = "top";
		c.fillText("FINISH", gs.width / 2, this.y + 10);
		c.restore();
	};

	this.update = function(gs) {
	};

	this.get_collision_aabb = function() {
		return [0, this.y, gs.width, 50];
	};

	this.collide_aabb = function(who, result) {
		if (who.y > this.y) {
			this.passed = true;
		}
	};
}

function Menu(world, gs) {
	this.priority = 5;
	statemachine(this);
	var that = this;

	var clearButtons = function() {
		$('#menu').css('display', 'None');
		$('#menu button').css('display', 'None')
			.prop('disabled', 'disabled');
		$('#menu .button').css('display', 'None')
			.prop('disabled', 'disabled');
		$('#name-form').css('display', 'None');
		$('#option-form').css('display', 'None');
	};
	var showButton = function(id) {
		$(id).css('display', 'block')
			.removeProp('disabled');
	};
	var showMenu = function(top) {
		gs.pause();
		clearButtons();
		if (top == null) {
			top = 195;
		}
		$('#menu').css('display', 'block').css('top', top + 'px');
	};
	$('#button_start').click(function(){
		that.set_state("hidden");
		var type = $('#option-form input:radio[name=game-type]:checked').val();
		world.set_type(type);
		world.restart();
	});
	$('#button_scores').click(function() {
		scores.showScores(function() {});
	});
	$('#button_restart').click(function() {
		that.set_state("hidden");
		world.restart();
	});
	$('#button_back').click(function() {
		that.set_state("main");
		gs.schedule(1, gs.pause);
		gs.launch();
	});

	this.init = function(gs) {
		$('#option-form input:radio[name=game-type][value=' + world.get_type() +']').prop('checked', true);
		this.set_state("main");
	};

	this.main_init = function() {
		showMenu(195-44);
		showButton('#button_start');
		showButton('#button_scores');
		showButton('#button_instructions');
		$('#option-form').css('display', 'block');
	};

	this.hidden_init = function() {
		clearButtons();
		gs.launch();
	};

	this.score_init = function() {
		showMenu();
		showButton('#button_submit');
		$('#name-form').css('display', 'block');
		showButton('#button_restart');
		showButton('#button_back');
		var time = world.get_total_time() * 100;
		var errors = world.get_missed_gates();
		scores.showEnterForm(time, errors, function() {
			that.set_state("main");
			gs.schedule(1, gs.pause);
			gs.launch();
		});
	};

	this.gameover_init = function() {
		showMenu();
		showButton('#button_restart');
		showButton('#button_back');
	};

	this.main_draw = function(c, gs) {
	};

	this.hidden_draw = function(c, gs) {
	};

	this.score_draw = function(c, gs) {
		c.save();
		c.fillStyle = "rgb(255, 114, 0)";
		roundRect(c, 50, 40, 380, 150, 25);
		c.fill();
		c.font = "28pt flubberregular";
		c.fillStyle = "yellow";
		var time = world.get_time();
		var missed_gates = world.get_missed_gates();
		var total = world.get_total_time();
		var left = gs.width/2 - 180;
		var right = gs.width/2 + 180;
		var show_time = function(t) {
			var cent = Math.round(t.mod(1) * 100).toString();
			if (cent.length == 1) {
				cent = "0" + cent;
			}
			var sec = Math.floor(t.mod(60)).toString();
			if (sec.length == 1) {
				sec = "0" + sec;
			}
			var min = Math.floor(t / 60).toString();
			return min + ":" + sec + "." + cent;
		};
		c.fillText("Time:", left, 85);
		c.fillText("Errors:", left, 125);
		c.fillText("Total:", left, 170);
		c.textAlign = "right";
		c.fillText(show_time(time), right, 85);
		c.fillText(missed_gates + " x 0:05.00", right, 125);
		c.fillText(show_time(total), right, 170);
		c.restore();
	};

	this.gameover_draw = function(c, gs) {
		c.save();
		c.fillStyle = "rgb(255, 114, 0)";
		roundRect(c, 50, 60, 380, 100, 25);
		c.fill();
		c.font = "42pt flubberregular";
		c.fillStyle = "yellow";
		c.textAlign = "center";
		c.fillText("GAME OVER", gs.width/2, 130);
		c.restore();
	};
}

function Dpad(gs, skier, x) {
	this.priority = 7;
	this.pos_x = x;
	this.pos_y = gs.height - 60;
	var radius = 20;
	this.pointer_down = false;
	this.pointer = [this.pos_x, this.pos_y];
	var radius_pointer = 15;
	this.max_extent = 40;
	var touch_id = null;
	var that = this;

	this.init = function(gs) {
	};

	this.draw = function(c, gs) {
		c.save();
		c.lineWidth = 4;
		c.beginPath();
		c.arc(this.pos_x, this.pos_y, radius, 0, 2 * Math.PI);
		c.stroke();
		if (this.pointer_down) {
			c.beginPath();
			c.arc(this.pointer[0], this.pointer[1], radius_pointer, 0, 2 * Math.PI);
			c.stroke();
		}
		c.restore();
	};

	var setTouchOrPointer = function() {
		if (gs.touches != null) {
			var touch = util.get_touch_with_id(gs.touches, touch_id);
			if (touch == null) {
				touch_id = null;
				var coords = util.page_coordinates(gs, [that.pos_x, that.pos_y]);
				touch = util.get_closest_touch_in_range(gs.touches, coords, 100);
			}
			if (touch != null) {
				touch_id = touch.identifier;
				var p = util.canvas_coordinates(gs, [touch.pageX, touch.pageY]);
				that.setPointer(p);
			}
		} else {
			touch_id = null;
		}
	};

	this.pointerDown = function(i) {
		setTouchOrPointer();
		if (touch_id != null) {
			this.pointer_down = true;
		}
	};

	this.pointerUp = function(i) {
		setTouchOrPointer();
		if (touch_id == null) {
			this.pointer_down = false;
			this.setPointer([this.pos_x, this.pos_y]);
		}
	};

	this.pointerMove = function() {
		setTouchOrPointer();
	};

	this.setPointer = function(p) {
		this.pointer = p;
	};

	this.pointerTest = function(pos) {
		return true;
	};
}

function LeftDpad(gs, skier) {
	var that = new Dpad(gs, skier, 60);

	that.update = function(gs) {
		if (that.pointer[0] > that.pos_x + 20 && that.pointer_down) {
			skier.keyHeld_39();
		}
		if (that.pointer[0] < that.pos_x - 20 && that.pointer_down) {
			skier.keyHeld_37();
		}
	};

	that.setPointer = function(p) {
		var x = Math.max(that.pos_x - that.max_extent, p[0]);
		x = Math.min(that.pos_x + that.max_extent, x);
		that.pointer = [x, that.pos_y];
	};

	return that;
}

function RightDpad(gs, skier) {
	var that = new Dpad(gs, skier, gs.width - 60);
	var reset_brake = false;
	var reset_accelerate = false;

	that.update = function(gs) {
		if (that.pointer[1] < that.pos_y - 20 && that.pointer_down) {
			skier.keyDown_38();
			reset_brake = true;
		} else {
			if (reset_brake) {
				skier.keyUp_38();
				reset_brake = false;
			}
		}
		if (that.pointer[1] > that.pos_y + 20 && that.pointer_down) {
			skier.keyDown_40();
			reset_accelerate = true;
		} else {
			if (reset_accelerate) {
				skier.keyUp_40();
				reset_accelerate = false;
			}
		}
	};

	that.setPointer = function(p) {
		var y = Math.max(that.pos_y - that.max_extent, p[1]);
		y = Math.min(that.pos_y + that.max_extent, y);
		that.pointer = [that.pos_x, y];
	};

	return that;
}

function PowerUp(gs, world, type, x, y) {
	this.priority = 2;
	this.world = world;
	this.x = x;
	this.y = y;
	this.pos = [x, y];
	var that = this;
	var sprite;
	var collect;
	if (type == 'slow') {
		sprite = new Sprite(["center", "center"],
				{ normal: [["images/slow.png"]] });
		collect = function() {
			that.world.remove_power_up(that);
			that.world.slowdown++;
			gs.schedule(50 * 5, function() {
				if (that.world.slowdown > 0) {
					that.world.slowdown--;
				}
			});
		};
	} else if (type == 'timebonus') {
		sprite = new Sprite(["center", "center"],
				{ normal: [["images/timebonus.png"]] });
		collect = function() {
			that.world.time_bonus(5);
			that.world.remove_power_up(that);
		};
	} else if (type == 'stone') {
		sprite = new Sprite(["center", "center"],
				{ normal: [["images/stone.png"]] });
		collect = this.world.gameover;
	} else if (type == 'gift') {
		sprite = new Sprite(["center", "center"],
				{ normal: [["images/gift.png"]] });
		collect = function() {
			that.world.time_bonus(2);
			that.world.remove_power_up(that);
		};
	} else {
		throw 'Invalid powerup type';
	}

	this.init = function(gs) {
	};

	this.is_visible = function() {
		return this.world.is_visible([this.x - 15,
		                            this.y - 15,
		                            this.x + 15,
		                            this.y + 15]);
	};

	this.draw = function(c, gs) {
		if (!this.is_visible()) {
			return;
		}

		c.save();
		this.world.cameraTransformation(c);
		sprite.draw(c, this.pos);
		c.restore();
	};

	this.update = function(gs) {
	};

	this.get_collision_circle = function() {
		return [this.pos, 15];
	};

	this.collide_circle = function(who, result) {
		collect();
	};
}

function Santa(world, gs, skier, dropX, dropY, onDrop) {
	var started = false;
	var droped = false;
	// set starting point
	var x = (dropX > gs.width / 2) ? -40 : gs.width + 40;
	var y = dropY - 700;
	var angle = Math.atan2(dropX - x, dropY - y);
	var sprite = new Sprite(["center", "center"], { normal: [["images/santa.png"]] });
	sprite.angle(-angle);
	var speed = 700;

	this.priority = 4;

	var doStart = function() {
		if (skier.y > y + 200) {
			started = true;
		}
		return started;
	};

	this.draw = function(c, gs) {
		if (started) {
			c.save();
			world.cameraTransformation(c);
			sprite.draw(c, [x, y]);
			c.restore();
		}
	};

	this.update = function(gs) {
		if (!started && !doStart()) {
			return;
		}
		if (!droped && y >= dropY + 50) {
			droped = true;
			onDrop();
		}
		var time = world.deltaT;
		// update with speed
		x = x + speed * Math.sin(angle) * time;
		y = y + speed * Math.cos(angle) * time;
	};
}

function World(gs) {
	this.priority = 0;
	this.cpos = gs.height / 2;
	this.gravity = 50;
	this.cw = 0.06;
	this.cwFast = 0.01;
	this.deltaT = 0;
	this.border = 40;
	this.slowdown = 0;

	var type = window.localStorage.getItem('game-type');
	if (type == null) {
		type = "mult";
	}
	this.get_type = function() {
		return type;
	};
	this.set_type = function(t) {
		type = t;
		window.localStorage.setItem('game-type', t);
	};

	var skier = new Skier(this);
	var gates = new Array();
	var quizes = [];
	var finishLine = new FinishLine(this, gs);
	var powerUps = [];
	var stopWatch = new StopWatch(this);
	var menu = new Menu(this, gs);
	var leftDpad = new LeftDpad(gs, skier);
	var rightDpad = new RightDpad(gs, skier);
	var lastTime = null;
	var that = this;

	var pause = function() {
		stopWatch.stop();
		skier.set_state("stand");
		leftDpad.pointer_down = false;
		rightDpad.pointer_down = false;
	};
	var start = function() {
		skier.start();
		stopWatch.start();
	};

	this.cameraTransformation = function(c) {
		c.translate(0, - this.cpos + gs.height / 2);
	};

	this.is_visible = function(box) {
		var top = this.cpos - gs.height / 2;
		var bottom = this.cpos + gs.height / 2;
		return top < box[3] && bottom > box[1];
	};

	this.get_time = function() {
		return stopWatch.seconds;
	};

	this.time_bonus = function(bonus) {
		stopWatch.seconds -= bonus;
	};

	this.get_total_time = function() {
		return this.get_time() + 5 * this.get_missed_gates();
	};

	this.get_missed_gates = function() {
		return gates.reduce(function(n, g){ return (g.passed || !g.correct_answer) ? n : n + 1; }, 0);
	};

	var clear_power_ups = function() {
		for ( var index = 0; index < powerUps.length; index++) {
			gs.delEntity(powerUps[index]);
		}
		powerUps = [];
	};

	var add_power_ups = function() {
		clear_power_ups();

		// create 1 stone, 2 timeboni and 3 slow down (6 slots)
		// timeboni are in the middle
		randomElements(19, 2).forEach(function(slot) {
			var xdiff = Math.floor((Math.random() * (101)) - 50);
			var ydiff = Math.floor((Math.random() * (51)) - 50);
			var timebonus = new PowerUp(gs, that, 'timebonus', gs.width/2 + xdiff, 400 + slot * 300 + ydiff);
			add_power_up(timebonus);
		});
		// stones and slowdown are left or right
		var slots = randomElements(28, 4);
		for ( var i = 0; i < slots.length; i++) {
			var slot = Math.floor(slots[i] / 2);
			var type = (i == 0) ? 'stone' : 'slow';
			var x = (slots[i] % 2 == 0) ? 145 : 345;
			var xdiff = Math.floor((Math.random() * (21)) - 10);
			var ydiff = Math.floor((Math.random() * (81)) - 40);
			var powerup = new PowerUp(gs, that, type, x + xdiff, 400 + slot * 300 + ydiff);
			add_power_up(powerup);
		}
		if (isXmas) {
			// santa
			var right = Math.random() > 0.5;
			var dropX = right ? 350 : 150;
			var dropY = 1100 + Math.floor(Math.random() * 15) * 300;
			var santa = new Santa(that, gs, skier, dropX, dropY, function() {
				var gift = new PowerUp(gs, that, 'gift', dropX, dropY);
				add_power_up(gift);
			});
			add_power_up(santa);
		}
	};

	var add_power_up = function(powerup) {
		powerUps.push(powerup);
		gs.addEntity(powerup);
	};

	this.remove_power_up = function(powerup) {
		var index = powerUps.indexOf(powerup);
		if (index >= 0) {
			powerUps.splice(index, 1);
		}
		gs.delEntity(powerup);
	};

	this.init = function(gs) {
		gs.addEntity(skier);
		gs.addEntity(stopWatch);
		gs.addEntity(finishLine);
		if (gs.isTouchDevice()) {
			gs.addEntity(leftDpad);
			gs.addEntity(rightDpad);
		}

		for (var i = 0; i < 20; i++) {
			var left = new Gate(this, 100, i * 300 + 300, 90, 1, true);
			var right = new Gate(this, 300, i * 300 + 300, 90, 1, false);

			gates.push(left);
			gates.push(right);
			gs.addEntity(left);
			gs.addEntity(right);
		}
		finishLine.y = 200 + 20 * 300;

		gs.schedule(2, function() { gs.addEntity(menu); });
	};

	this.restart = function() {
		this.cpos = gs.height / 2;
		skier.reset(gs);
		finishLine.passed = false;
		var gen = quiz_gen(type);
		var quiz = null;
		quizes = [];
		for (var i = 0; i < gates.length; i++) {
			if (i % 2 == 1) {
				gates[i].answer = quiz.right_answer();
				gates[i].correct_answer = quiz.right;
			} else {
				quiz = gen.next();
				quizes.push(quiz);
				gates[i].answer = quiz.left_answer();
				gates[i].correct_answer = !quiz.right;
			}
			gates[i].passed = false;
		}
		stopWatch.reset();
		add_power_ups();
		this.slowdown = 0;
		setTimeout(start, 2000);
	};

	var trees_left;
	if (isXmas) {
		trees_left = new Sprite(["left", "top"],{ normal : [["images/xmas_trees_left_01.png", 18],
		                                                    ["images/xmas_trees_left_02.png", 18],
		                                                    ["images/xmas_trees_left_03.png", 18],
		                                                    ["images/xmas_trees_left_04.png", 18],
		                                                    ["images/xmas_trees_left_05.png", 18]] });
	} else {
		trees_left = new Sprite(["left", "top"],{ normal : [["images/trees_left.png"]] });
	}
	var trees_right;
	if (isXmas) {
		trees_right = new Sprite(["right", "top"],{ normal : [["images/xmas_trees_right_02.png", 7],
		                                                     ["images/xmas_trees_right_04.png", 17],
		                                                     ["images/xmas_trees_right_01.png", 17],
		                                                     ["images/xmas_trees_right_05.png", 17],
		                                                     ["images/xmas_trees_right_03.png", 17],
		                                                     ["images/xmas_trees_right_02.png", 10]] });
	} else {
		trees_right = new Sprite(["right", "top"],{ normal : [["images/trees_right.png"]] });
	}

	this.draw = function(c, gs) {
		c.save();
		this.cameraTransformation(c);
		c.fillStyle = "rgb(11,124,51)";
		var tile_height = trees_left.get_size()[1] - 1;
		var n = Math.ceil(gs.height / tile_height) + 1;
		var start = Math.floor((this.cpos - gs.height / 2) / tile_height) * tile_height;
		for (var i = 0; i < n; i++) {
			trees_left.draw(c, [0, start + i * tile_height]);
			trees_right.draw(c, [gs.width, start + i * tile_height]);
		}
		c.font = "18pt Arial"; 
		c.textAlign = "center";
		for ( var j = 0; j < quizes.length; j++) {
			var y = 150 + 300 * j;
			if (this.is_visible([0, y - 20, 0, y])) {
				c.fillText(quizes[j].toString(), gs.width / 2, y);
			}
		}
		c.restore();
	};

	this.update = function(gs) {
		// update camera
		if (skier.y + gs.width/5 > this.cpos && this.cpos < finishLine.y) {
			this.cpos = skier.y + gs.width/5;
		}

		// update deltaT
		var time = new Date().getTime();
		if (lastTime) {
			// do not update deltaT when the game was paused
			if (time - lastTime < 500) {
				this.deltaT = (time - lastTime) / 1000;
			}
		} else {
			this.deltaT = 0;
		}
		lastTime = time;
		trees_left.update();
		trees_right.update();

		// collisions
		if (skier.state == "ski") {
			collide.aabb(gates, [skier]);
			collide.aabb([finishLine], [skier]);
			collide.circles(powerUps, [skier]);

			if (finishLine.passed) {
				pause();
				menu.set_state("score");
			}
			if (skier.x < this.border || skier.x > gs.width - this.border) {
				this.gameover();
			}
		}
	};

	this.gameover = function() {
		pause();
		menu.set_state("gameover");
	};
}

function startGame(gs) {
	// preload sprites
	var sprites = ["images/slow.png",
	                "images/stone.png",
	                "images/timebonus.png",
	                "images/empty.png"];
	if (isXmas) {
		sprites = sprites.concat(
					["images/santa.png",
					 "images/gift.png",
					 "images/cane_blue_l.png",
					 "images/cane_blue_r.png",
					 "images/cane_red_l.png",
					 "images/cane_red_r.png",
					 "images/xmas_skier.png",
					 "images/xmas_skier_braking.png",
					 "images/xmas_skier_fast.png",
					 "images/xmas_skier_start01.png",
					 "images/xmas_skier_start02.png",
					 "images/xmas_skier_start03.png",
					 "images/xmas_skier_start04.png",
					 "images/xmas_skier_start05.png",
					 "images/xmas_skier_start06.png",
					 "images/xmas_skier_start07.png",
					 "images/xmas_trees_left_01.png",
					 "images/xmas_trees_left_02.png",
					 "images/xmas_trees_left_03.png",
					 "images/xmas_trees_left_04.png",
					 "images/xmas_trees_left_05.png",
					 "images/xmas_trees_right_01.png",
					 "images/xmas_trees_right_02.png",
					 "images/xmas_trees_right_03.png",
					 "images/xmas_trees_right_04.png",
					 "images/xmas_trees_right_05.png"
					 ]);
	} else {
		sprites = sprites.concat(
					["images/skier.png",
					 "images/skier_braking.png",
					 "images/skier_fast.png",
					 "images/skier_start01.png",
					 "images/skier_start02.png",
					 "images/skier_start03.png",
					 "images/skier_start04.png",
					 "images/skier_start05.png",
					 "images/skier_start06.png",
					 "images/skier_start07.png",
					 "images/flagb01.png",
					 "images/flagb02.png",
					 "images/flagb03.png",
					 "images/flagb04.png",
					 "images/flagb05.png",
					 "images/flagb06.png",
					 "images/flagb07.png",
					 "images/flagb08.png",
					 "images/flagr01.png",
					 "images/flagr02.png",
					 "images/flagr03.png",
					 "images/flagr04.png",
					 "images/flagr05.png",
					 "images/flagr06.png",
					 "images/flagr07.png",
					 "images/flagr08.png",
					 "images/trees_left.png",
					 "images/trees_right.png"
					 ]);
	}
	Sprite.preload(sprites, function() { gs.addEntity(new World(gs)); });
}
