/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.rest.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.RollbackException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.corten.aha.scores.data.Score;

@Path("/worker/scores/update")
public class UpdateScoreResource {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateScoreResource.class);

    @PersistenceUnit
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Scores");

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public Response update(String data) {
        LOG.debug("Data: [" + data + "]");
        EntityManager em = emf.createEntityManager();
        try {
            String[] items = data.split(",");
            for (String item : items) {
                updateItem(em, item);
            }
        } finally {
            em.close();
        }
        return Response.noContent().build();
    }

    private void updateItem(EntityManager em, String item) {
        try {
            em.getTransaction().begin();
            String[] properties = item.split(":");
            long id = Long.parseLong(properties[0]);
            int rank = Integer.parseInt(properties[1]);
            Score entity = em.find(Score.class, id);
            entity.setRank(rank);
            em.persist(entity);
            em.getTransaction().commit();
        } catch (RollbackException e) {
            LOG.error("Failed to update rank for score " + item, e);
        }
    }
}
