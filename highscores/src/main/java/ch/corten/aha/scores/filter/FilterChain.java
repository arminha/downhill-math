/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.filter;

import java.util.ArrayList;
import java.util.List;

public class FilterChain implements Filter {

    private final List<Filter> filters = new ArrayList<Filter>();

    public FilterChain(Filter... filters) {
        for (Filter filter : filters) {
            this.filters.add(filter);
        }
    }

    @Override
    public String sanitize(String s) {
        String result = s;
        for (Filter filter : filters) {
            result = filter.sanitize(result);
        }
        return result;
    }

}
