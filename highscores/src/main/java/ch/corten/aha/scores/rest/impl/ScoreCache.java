/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.rest.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.corten.aha.scores.rest.Score;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

public class ScoreCache {

    private static final int RECENT_SCORE_CACHE_TIMEOUT = 120;

    private static final Logger LOG = LoggerFactory.getLogger(ScoreCache.class);

    private static final String LIST_CACHE_KEY = "scores";
    private static final String RECENT_CREATED_PREFIX = "recentScore-";

    public void putHighScoreList(List<Score> results) {
        if (results != null) {
            LOG.trace("Store scores in cache");
        } else {
            LOG.trace("Clear scores from cache");
        }
        MemcacheServiceFactory.getAsyncMemcacheService().put(LIST_CACHE_KEY, results);
    }

    @SuppressWarnings("unchecked")
    public List<Score> getHighScoreList() {
        return (List<Score>) MemcacheServiceFactory.getMemcacheService().get(LIST_CACHE_KEY);
    }

    public Long getRecentlyCreated(Score score) {
        return (Long) MemcacheServiceFactory.getMemcacheService().get(getKey(score));
    }

    public void putRecentlyCreated(Score score, long time) {
        MemcacheServiceFactory.getMemcacheService().put(getKey(score), Long.valueOf(time),
                Expiration.byDeltaSeconds(RECENT_SCORE_CACHE_TIMEOUT));
    }

    private String getKey(Score score) {
        return RECENT_CREATED_PREFIX + score.toString();
    }

}
