/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.rest.impl;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.corten.aha.scores.filter.Filter;
import ch.corten.aha.scores.filter.HtmlFilter;
import ch.corten.aha.scores.logging.AdminLog;
import ch.corten.aha.scores.logging.AdminLog.Level;
import ch.corten.aha.scores.rest.Score;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

@Path("/scores")
public class ScoreResource {

    private static final int MAX_LIST_SIZE = 200;

    private static final Logger LOG = LoggerFactory.getLogger(ScoreResource.class);

    @Context
    private UriInfo uri;

    @PersistenceUnit
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Scores");

    private final ScoreCache cache = new ScoreCache();
    private final Filter filter = new HtmlFilter();

    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public List<Score> list() {
        List<Score> scores = cache.getHighScoreList();
        if (scores != null) {
            LOG.trace("Return scores from cache.");
            return scores;
        }
        EntityManager em = emf.createEntityManager();
        try {
            Query query = em.createQuery("select s from Score s ORDER BY s.time ASC, s.errors ASC");
            query.setMaxResults(MAX_LIST_SIZE);
            @SuppressWarnings("unchecked")
            List<ch.corten.aha.scores.data.Score> list = query.getResultList();
            List<Score> results = new ArrayList<Score>();
            List<ch.corten.aha.scores.data.Score> update = new ArrayList<ch.corten.aha.scores.data.Score>();
            int rank = 1;
            for (ch.corten.aha.scores.data.Score entity : list) {
                if (entity.getRank() != rank) {
                    LOG.debug("update rank of score: " + entity.toString());
                    entity.setRank(rank);
                    update.add(entity);
                }
                results.add(fromEntity(entity));
                rank++;
            }
            if (!update.isEmpty()) {
                scheduleUpdateTasks(update);
            }
            cache.putHighScoreList(results);
            return results;
        } finally {
            try {
                em.close();
            } catch (PersistenceException e) {
                LOG.warn("Error closing EntityManager", e);
            }
        }
    }

    private static final int MAX_UPDATES_PER_TASK = 20;

    private void scheduleUpdateTasks(List<ch.corten.aha.scores.data.Score> update) {
        Queue queue = QueueFactory.getDefaultQueue();
        Iterator<ch.corten.aha.scores.data.Score> iterator = update.iterator();
        String url = uri.getBaseUriBuilder().path(UpdateScoreResource.class).build().getPath();
        while (iterator.hasNext()) {
            // create one task for 20 updates
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (iterator.hasNext() && i < MAX_UPDATES_PER_TASK) {
                ch.corten.aha.scores.data.Score entity = iterator.next();
                sb.append(entity.getId());
                sb.append(':');
                sb.append(entity.getRank());
                sb.append(',');
                i++;
            }
            LOG.trace("Add task with url: [" + url + "] - data: [" + sb.toString() + "]");
            queue.add(withUrl(url).method(Method.PUT).payload(sb.toString()));
        }
    }

    @POST
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Score create(Score score) {
        if (isDoubleEntry(score)) {
            LOG.warn("Ignore double entry into scrores table: " + score);
            return score;
        }
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            computeRank(em, score);
            LOG.info("Add new score: " + score);
            sanitize(score);
            ch.corten.aha.scores.data.Score entity = toEntity(score);
            entity.setCreatedAt(new Date());
            em.persist(entity);
            // updateOtherRanks(em, entity);
            em.getTransaction().commit();
            cache.putHighScoreList(null);
            return score;
        } finally {
            em.close();
        }
    }

    private static final int DOUBLE_ENTRY_LIMIT = 20000;
    private static final int POSSIBLE_DOUBLE_ENTRY_LIMIT = 60000;

    private boolean isDoubleEntry(Score score) {
        Long lastCreatedTime = cache.getRecentlyCreated(score);
        if (lastCreatedTime != null) {
            long difference = System.currentTimeMillis() - lastCreatedTime;
            if (difference < DOUBLE_ENTRY_LIMIT) {
                return true;
            } else if (difference < POSSIBLE_DOUBLE_ENTRY_LIMIT) {
                AdminLog.sendMail(Level.Warning, "Possible double entries in high score table. Check score " + score);
            }
        }
        cache.putRecentlyCreated(score, System.currentTimeMillis());
        return false;
    }

    private void sanitize(Score score) {
        String name = score.getName();
        name = filter.sanitize(name);
        score.setName(name);
    }

    private static void computeRank(EntityManager em, Score score) {
        // this is not accurate, but good enough as it will be corrected in the
        // list method if it is wrong.
        // The appengine datastore cannot handle a query that gives accurate results,
        // cause it can only have equals expressions in an or term.
        Query query = em.createQuery("SELECT s FROM Score s "
                + "WHERE s.time <= :time "
                + "ORDER BY s.time DESC, s.errors DESC");
        query.setParameter("time", score.getTime());
        query.setMaxResults(1);
        @SuppressWarnings("unchecked")
        List<ch.corten.aha.scores.data.Score> result = query.getResultList();
        if (result.size() > 0) {
            score.setRank(result.get(0).getRank() + 1);
        } else {
            score.setRank(1);
        }
    }

    private static Score fromEntity(ch.corten.aha.scores.data.Score entity) {
        Score score = new Score();
        score.setName(entity.getName());
        score.setRank(entity.getRank());
        score.setTime(entity.getTime());
        score.setErrors(entity.getErrors());
        return score;
    }

    private static ch.corten.aha.scores.data.Score toEntity(Score score) {
        ch.corten.aha.scores.data.Score entity = new ch.corten.aha.scores.data.Score();
        entity.setName(score.getName());
        entity.setRank(score.getRank());
        entity.setTime(score.getTime());
        entity.setErrors(score.getErrors());
        return entity;
    }
}
