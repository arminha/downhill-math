/*
 * Copyright (C) 2013  Armin Häberling
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ch.corten.aha.scores.logging;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.mail.MailService.Message;
import com.google.appengine.api.mail.MailServiceFactory;

public final class AdminLog {

    private AdminLog() {
    }

    /**
     * Log level for AdminLog.
     */
    public enum Level {
        Info,
        Warning,
        Error
    }

    private static final Logger LOG = LoggerFactory.getLogger(AdminLog.class);

    private static final String PROPERTIES = "/admin.properties";
    private static final String SENDER_KEY = "admin.log.mail.sender";

    public static void sendMail(Level level, String message) {
        try {
            Message msg = new Message();
            msg.setSubject("Downhill Math: " + level);
            msg.setSender(getSender());
            msg.setTextBody(message);
            MailServiceFactory.getMailService().sendToAdmins(msg);
        } catch (IOException e) {
            LOG.error("Unable to send message to admin: " + e.getMessage(), e);
        }
    }

    private static String getSender() throws IOException {
        Properties props = new Properties();
        try (InputStream stream = AdminLog.class.getResourceAsStream(PROPERTIES)) {
            props.load(stream);
        }
        return props.getProperty(SENDER_KEY);
    }

}
