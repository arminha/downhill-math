Downhill Math
=============

A simple skiing math game written in HTML and JavaScript.

Credits
-------

Idea by vyick and arminha.

Programming by arminha.

Downhill Math is built on top of the following libraries:

* `jsGameSoup <https://github.com/chr15m/jsGameSoup>`__
* `jQuery <http://jquery.com/>`__
* `jQueryTools Overlay <http://jquerytools.org/documentation/overlay/index.html>`__

The high scores back end is a REST API built with `RESTEasy <http://www.jboss.org/resteasy>`__ and running on `Google App Engine <https://developers.google.com/appengine>`__.

It uses the Flubber font from `Unauthorized Type <http://uatype.faithweb.com/>`__.
The webfonts from it were generated with the `Font Squirrel Webfont Generator <http://www.fontsquirrel.com/tools/webfont-generator>`__.

And last but not least the background image is from vyick.
